using System;

class exercise3
{
   static void Main()
   {
      double hundredAmount;
      double fiftyAmount;
      double twentyAmount;
      double tenAmount;
      double fiveAmount;
      double twoAmount;
      double oneAmount;

      double number = double.Parse(Console.ReadLine());
      
      hundredAmount = number / 100; number = number % 100;
      fiftyAmount = number / 50; number = number % 50;
      twentyAmount = number / 20; number = number % 20;
      tenAmount = number / 10; number = number % 10;
      fiveAmount = number / 5; number = number % 5;
      twoAmount = number / 2; number = number % 2;
      oneAmount = number / 1; number = number % 1;

      Console.WriteLine("NOTAS:");
      Console.WriteLine($"{(int)hundredAmount} nota(s) de R$ 100,00");
      Console.WriteLine($"{(int)fiftyAmount} nota(s) de R$ 50,00");
      Console.WriteLine($"{(int)twentyAmount} nota(s) de R$ 20,00");
      Console.WriteLine($"{(int)tenAmount} nota(s) de R$ 10,00");
      Console.WriteLine($"{(int)fiveAmount} nota(s) de R$ 5,00");
      Console.WriteLine($"{(int)twoAmount} nota(s) de R$ 2,00");

      double zeroFifty;
      double zeroTwentyFive;
      double zeroTen;
      double zeroFive;
      double zeroOne;

      zeroFifty = number / 0.5; number = number % 0.5;
      zeroTwentyFive = number / 0.25; number = number % 0.25;
      zeroTen = number / 0.10; number = number % 0.10;
      zeroFive = number / 0.05; number = number % 0.05;
      zeroOne = number / 0.01; number = number % 0.01;
     
      Console.WriteLine("MOEDAS:");
      Console.WriteLine($"{(int)oneAmount} moeda(s) de R$ 1,00");
      Console.WriteLine($"{(int)zeroFifty} moeda(s) de R$ 0.50");
      Console.WriteLine($"{(int)zeroTwentyFive} moeda(s) de R$ 0.25");
      Console.WriteLine($"{(int)zeroTen} moeda(s) de R$ 0.10");
      Console.WriteLine($"{(int)zeroFive} moeda(s) de R$ 0.05");
      Console.WriteLine($"{(int)zeroOne} moeda(s) de R$ 0.01");
   }
}
