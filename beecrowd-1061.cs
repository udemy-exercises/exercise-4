using System;

class exercise4
{
   static void Main()
   {
      string[] dayOneString = Console.ReadLine().Split(' ');
      int dayOne = int.Parse(dayOneString[1]);

      string[] timeOneString = Console.ReadLine().Split(':');
     
      string[] dayTwoString = Console.ReadLine().Split(' ');
      int dayTwo = int.Parse(dayTwoString[1]);

      string[] timeTwoString = Console.ReadLine().Split(':');

      (int second, int minute, int hour) timeOne;
      (int second, int minute, int hour) timeTwo;

      timeOne.hour = int.Parse(timeOneString[0]);
      timeOne.minute = int.Parse(timeOneString[1]);
      timeOne.second = int.Parse(timeOneString[2]);      

      timeTwo.hour = int.Parse(timeTwoString[0]);
      timeTwo.minute = int.Parse(timeTwoString[1]);
      timeTwo.second = int.Parse(timeTwoString[2]);


      int totalSecondsOne = (timeOne.hour * 3600) + (timeOne.minute * 60) + timeOne.second;
      int totalSecondsTwo = (timeTwo.hour * 3600) + (timeTwo.minute * 60) + timeTwo.second;
      int totalSeconds = 86400 * (dayTwo - dayOne);
      totalSeconds -= totalSecondsOne - totalSecondsTwo;

      int days;
      int hours;
      int minutes;
      int seconds;
   
      days = totalSeconds / 86400; totalSeconds = totalSeconds % 86400;
      hours = totalSeconds / 3600; totalSeconds = totalSeconds % 3600;
      minutes = totalSeconds / 60; totalSeconds = totalSeconds % 60;
      seconds = totalSeconds / 1; totalSeconds = totalSeconds % 1;

      Console.WriteLine($"{days} dia(s)");
      Console.WriteLine($"{hours} hora(s)");
      Console.WriteLine($"{minutes} minuto(s)");
      Console.WriteLine($"{seconds} segundo(s)");
   }
}
