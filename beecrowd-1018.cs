using System;

class exercise3
{
   static void Main()
   {
      int hundredAmount;
      int fiftyAmount;
      int twentyAmount;
      int tenAmount;
      int fiveAmount;
      int twoAmount;
      int oneAmount;

      int number = int.Parse(Console.ReadLine());
      
      hundredAmount = number / 100; number = number % 100;
      fiftyAmount = number / 50; number = number % 50;
      twentyAmount = number / 20; number = number % 20;
      tenAmount = number / 10; number = number % 10;
      fiveAmount = number / 5; number = number % 5;
      twoAmount = number / 2; number = number % 2;
      oneAmount = number / 1; number = number % 1;

      Console.WriteLine($"{hundredAmount} nota(s) de R$ 100,00");
      Console.WriteLine($"{fiftyAmount} nota(s) de R$ 50,00");
      Console.WriteLine($"{twentyAmount} nota(s) de R$ 20,00");
      Console.WriteLine($"{tenAmount} nota(s) de R$ 10,00");
      Console.WriteLine($"{fiveAmount} nota(s) de R$ 5,00");
      Console.WriteLine($"{twoAmount} nota(s) de R$ 2,00");
      Console.WriteLine($"{oneAmount} nota(s) de R$ 1,00");
   }
}
