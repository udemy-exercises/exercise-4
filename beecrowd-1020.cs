using System;

class exercise4
{
   static void Main()
   {
      int years;
      int months;
      int days;

      int number = int.Parse(Console.ReadLine());

      years = number / 365; number = number % 365;
      months = number / 30; number = number % 30;
      days = number / 1; number = number % 1;

      Console.WriteLine($"{years} ano(s)");
      Console.WriteLine($"{months} mes(es)");
      Console.WriteLine($"{days} dia(s)");
   }
}
